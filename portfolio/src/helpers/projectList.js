import project01 from "./../img/projects/01.jpg";
import project02 from "./../img/projects/02.jpg";
import project03 from "./../img/projects/03.jpg";

const projects = [
  {
    title: 'Food Shop',
    skils: 'React',
    img: project01,
    gitLabLink: 'https://gitlab.com/my_project75/module-react'
  }, 
  {
    title: 'Visit card',
    skils: 'Html,CSS',
    img: project02,
    gitLabLink: 'https://gitlab.com/my_project75/card'
  },
  {
    title: 'Website for ordering window installation',
    skils: 'Html,CSS',
    img: project03,
    gitLabLink: 'https://gitlab.com/my_project75/layout_practice'
  },
]

export {projects};